$(document).ready(() => {

	$("#options").on("submit", () => {

		const settings = {};

		$.each($("#options").serializeArray(), (i, a) => {
			settings[a.name] = a.value;
		});

		chrome.storage.sync.set({'settings' : settings}, () => {
			console.log("settings saved");

			$("#message").html("Data saved.");
		});

		return false;
	});

	$.post("https://fakecake.org/spellbot/urlposter.php", {listowners: 1}, (owners) => {
		const select = $("#owner");

		$.each(owners, (i, owner) => {
			select.append($("<option>")
				.val(owner)
				.text(owner));
		});

		chrome.storage.sync.get('settings', (ret) => {
			if (typeof ret.settings !== "undefined") {
				const settings = ret.settings;

				$.each(settings, (k, v) => {
					$("#" + k).val(v);
				});
			}
		});
	});
});
