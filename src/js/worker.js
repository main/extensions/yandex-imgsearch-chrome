chrome.runtime.onInstalled.addListener(() => {
	chrome.contextMenus.create({"title": "Reverse image search",
		"contexts":[ "link", "image" ],
		"id": "context-menu-yandex"
	});
});

chrome.contextMenus.onClicked.addListener((info, tabs) => {
	const url = info["srcUrl"] ? info["srcUrl"] : info["linkUrl"];

	if (url) {
		const query = "https://yandex.ru/images/search?url="+encodeURIComponent(url)+"&rpt=imagelike";

		chrome.tabs.create({url : query});
	}
});
